cmake_minimum_required(VERSION 3.0)
project(InitializerList)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp)
add_executable(InitializerList ${SOURCE_FILES})