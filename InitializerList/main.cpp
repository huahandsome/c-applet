#include <iostream>

class MyClass {
    public:
        // Reference member, has to be Initialized in Member Initializer List
        int &i;
        int b;
        // Non static const member, must be Initialized in Memnber Initializer List
        const int k;

        MyClass( int a, int b, int c ): i(a), b(b), k(c){}

        // ERROR Init!!!!!!!!!
        //MyClass( int a, int b, int c ){
        //    i = a;
        //    this->b = b;
        //    k = c;
        //}
};

int main() {
    int x = 10;
    int y = 20;
    int z = 30;

    MyClass obj(x, y, z);

    return 0;
}