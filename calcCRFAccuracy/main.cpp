#include <stdexcept>
#include "main.h"

using namespace std;

int main() {
    ifstream rstFile(FILE_PATH);
    unsigned int totalLine = 0;
    unsigned int matchLine = 0;
    string line;
    vector<string> vec;
    unsigned int dbgSize = 0;

    if ( rstFile.is_open() ) {
        while( getline( rstFile, line ) ) {
            totalLine++;
            dbgSize = splitWords( line, vec, DELIMITER );

            if (dbgSize < min(EXPECTED_RST_IN_COL, RUNNING_RST_IN_COL) ) {  // whitespace line
                //cout << "error line number: " << totalLine << endl;
                continue;
            }

            if ( rstIsMatch( vec, EXPECTED_RST_IN_COL, RUNNING_RST_IN_COL ) ) {
                matchLine++;
            }
        }

        cout << "the accuracy is: " << ((double)matchLine)/totalLine <<endl;

        rstFile.close();
    }

    return 0;
}

bool rstIsMatch( const vector<string>& vecStr, int expectedRstColumn, int runningRstColumn ) {
    bool ret = false;
    string expectedStr;
    string rstStr;

    // in case null vector
    // COMMENT OUT SINCE AT() WILL AUTOMATICALLY CHECK WHETHER THE INDEX IS OUT_OF_RANGE, AND THROW ERROR

    //if ( vecStr.size() >= ( max(expectedRstColumn, runningRstColumn) ) ) {
    try {
        expectedStr = vecStr.at( expectedRstColumn );
        rstStr      = vecStr.at( runningRstColumn );
        if ( !rstStr.compare(expectedStr) ) {
            ret = true;
        }
    } catch ( const out_of_range& oor ) {
        std::cerr << "Out of Range error:...... " << oor.what() << endl;
        std::cerr << "Size: " << vecStr.size() << endl;
    }

    //}
    return ret;
}

unsigned int splitWords( const string &txt, vector<string> &strs, char delimiter ) {
    unsigned int pos = txt.find( delimiter );
    unsigned int initialPos = 0;
    strs.clear();

    while( pos != string::npos ) {
        strs.push_back ( txt.substr( initialPos, pos - initialPos /*+ 1*/ ) );
        initialPos = pos + 1;

        pos = txt.find( delimiter, initialPos );
    }

    // Add the last one
    strs.push_back( txt.substr( initialPos, min( pos, txt.size()) - initialPos /*+ 1*/ ));

    return strs.size();
}

