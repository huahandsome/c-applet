//
// Created by huahandsome on 6/19/17.
//

#ifndef CALCACCURACY_MAIN_H
#define CALCACCURACY_MAIN_H

#include <iostream>
#include <fstream>
#include <vector>

#define FILE_PATH  "/home/huahandsome/Git/c-applet/CRF++-0.58/example/segTest2/test.rst"
#define EXPECTED_RST_IN_COL 2
#define RUNNING_RST_IN_COL  3
#define DELIMITER   '\t'

bool rstIsMatch( const std::vector<std::string>& inStr, int expectedRstColumn, int runningRstColumn );
unsigned int splitWords( const std::string &txt, std::vector<std::string> &strs, char ch );

#endif //CALCACCURACY_MAIN_H
