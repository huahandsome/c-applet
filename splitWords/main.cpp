#include <iostream>
#include <vector>

using namespace std;

unsigned int split( const string &txt, vector<string> &strs, char ch );
void showVec( const vector<string> &vec );

int main() {
    string str = "Hello Word, Hello San Jose, Hello United States";
    vector<string> vec;

    split( str, vec, ' ');
    showVec( vec );
    return 0;
}

unsigned int split( const string &txt, vector<string> &strs, char ch ) {
    unsigned int pos = txt.find( ch );
    unsigned int initialPos = 0;
    strs.clear();

    while( pos != string::npos ) {
        strs.push_back ( txt.substr( initialPos, pos - initialPos + 1 ) );
        initialPos = pos + 1;

        pos = txt.find( ch, initialPos );
    }

    // Add the last one
    strs.push_back( txt.substr( initialPos, min( pos, txt.size()) - initialPos + 1));

    return strs.size();
}

void showVec( const vector<string> &vec ) {
    vector<string>::const_iterator it = vec.begin();
    for ( ; it < vec.end(); it++ ) {
        cout << *it << endl;
    }
}