// This program demonstrates the vector pop_back member function.
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector<int> values;

    // Store values in the vector.
    values.push_back(1); // Last element in values is 1
    values.push_back(2); // Now elements in values are 1,2
    values.push_back(3); // Now elements in values are 1,2,3

    cout << "The size of values is " << values.size() << endl; // values has 3 elements

    // Remove a value from the vector.
    cout << "Popping a value from the vector...\n";
    values.pop_back();
    cout << "The size of values is now " << values.size() << endl; // 1 is Removed thus size is 2

    cout << values[0] << " " << values[1] << " " << values[2] << endl;

    // !!!!!!!!!!  THE ABOVE CODE IS INCORRECT.   !!!!!!!!!!
    //
    // https://stackoverflow.com/questions/32577576/when-using-vectors-does-pop-back-remove-values-along-with-elements
    //
    // "values" and "elements" are the same thing. pop_back() removes the last value from the vector, if the vector is not empty.
    //
    // vector is designed so that it is the programmer's responsibility to not access out of bounds of the vector. If a vector has 2 elements and you try to access the third element via any method except for at(), you cause undefined behaviour.
    //
    // To get bounds checking, use values.at(0) instead of values[0], etc., and include a try...catch block to catch the resulting exception.
    //
    // CHANGE TO USE "at()" instead, since at() will automatically check the bound.
    //
    // cout << values.at(0) << " " << values.at(1) << " " << values.at(2) << endl;

    // Now remove another value from the vector.
    cout << "Popping a value from the vector...\n";
    values.pop_back();
    cout << "The size of values is now " << values.size() << endl;
    cout << values[0] << " " << values[1] << " " << values[2] << endl;

    // Remove the last value from the vector.
    cout << "Popping a value from the vector...\n";
    values.pop_back();
    cout << "The size of values is now " << values.size() << endl;
    cout << values[0] << " " << values[1] << " " << values[2] << endl;

    return 0;
}
