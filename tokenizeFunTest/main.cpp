#include <iostream>
#include <cstring>
#include <algorithm>

size_t tokenize2(char *str, const char *del, /*Iterator out,*/ size_t max);

int main() {
    //std::string s_1 = "ssissippi";
    //std::string s   = "si";
    //size_t pos = s_1.find_first_of(s);

    //while( pos != std::string::npos ) {
    //    std::cout << pos << std::endl;
    //    pos++;
    //    pos = s_1.find_first_of( s, pos );
    //}

    char str[] = "sound blaster   B   O";
    char del[] = "\t ";

    tokenize2( str, del, 3);
    return 0;
}

size_t tokenize2(char *str, const char *del, /*Iterator out,*/ size_t max) {
    char *stre = str + std::strlen(str);
    const char *dele = del + std::strlen(del);
    size_t size = 0;

    while (size < max) {
        char *n = std::find_first_of(str, stre, del, dele);
        *n = '\0';
        if (*str != '\0') {
            //*out++ = str;
            ++size;
        }
        if (n == stre) break;
        str = n + 1;
    }

    return size;
}
